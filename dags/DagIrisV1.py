from airflow import DAG
from datetime import datetime, timedelta
from airflow.operators.dummy import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.utils.task_group import TaskGroup
from airflow.operators.python_operator import PythonOperator

pathScript = "/home/mlops/airflow/dags/etl_scripts"
pathIris =  "/home/mlops/airflow/dags/etl_scripts/featurestore/iris.txt"
pathEncoder = "/home/mlops/airflow/dags/etl_scripts/featurestore/irisEncoder.txt"

def task_failure_alert(context):
    subject = "[Airflow] DAG {0} - Task {1}: Failed".format(
        context['task_instance_key_str'].split('__')[0], 
        context['task_instance_key_str'].split('__')[1]
        )
    html_content = """
    DAG: {0}<br>
    Task: {1}<br>
    Failed on: {2}
    """.format(
        context['task_instance_key_str'].split('__')[0], 
        context['task_instance_key_str'].split('__')[1], 
        datetime.now()
        )
    #send_email_smtp(dag_vars["dev_mailing_list"], subject, html_content)    
    text_file = open("/home/mlops/airflow/dags/etl_scripts/erro.txt", "w")
    n = text_file.write(subject + html_content)
    text_file.close()
default_args = {
   'owner': 'teste',
   'depends_on_past': False,
   'start_date': datetime(2019, 1, 1),
   'retries': 0,
   'on_failure_callback': task_failure_alert
   }
with DAG(
   'dag-pipeline-iris-aula-ia-v1',
   schedule_interval=timedelta(minutes=1000),
   catchup=False,
   default_args=default_args
   ) as dag:
    start = DummyOperator(task_id="start")
    with TaskGroup("etl", tooltip="etl") as etl:       
        t1 = BashOperator(
            dag=dag,
            task_id='download_dataset',
            bash_command="""
            cd {0}/featurestore
            gdown https://drive.google.com/uc?id=1rZgVuwYon_3QogTr0-v480PRpi-2l1-v
            """.format(pathScript)
        )
        [t1]
    with TaskGroup("preProcessing", tooltip="preProcessing") as preProcessing:
        t2 = BashOperator(
            dag=dag,
            task_id='clear_dataset',
            bash_command="""
            cd {0}
            python etl_preprocessing.py {1} {2}
            """.format(pathScript, pathIris, pathEncoder)
        )
        [t2]
    with TaskGroup("modelProcessing", tooltip="modelProcessing") as modelProcessing:
        t3 = BashOperator(
            dag=dag,
            task_id='algoritmo1',
            bash_command="""
            cd {0}
            python model_processing.py {1} {2}
            """.format(pathScript, pathIris, pathEncoder)
        )
        t4 = BashOperator(
            dag=dag,
            task_id='algoritmo2',
            bash_command="""
            cd {0}
            python model_processing_dirty.py {1} {2}
            """.format(pathScript, pathIris, pathEncoder)
        )       
        [t3,t4]
    end = DummyOperator(task_id='end')
    start >> etl >> preProcessing >> modelProcessing >> end