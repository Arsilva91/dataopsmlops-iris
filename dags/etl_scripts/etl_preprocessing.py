from asyncio import constants
import chunk
from crypt import methods
import pandas as pd
from sklearn import preprocessing
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("inputPath", help="arquivo input Iris", type=str)
parser.add_argument("outputPath", help="arquivo output Iris", type=str)
args = parser.parse_args()
columns = ['sepal_length','sepal_width','petal_length','petal_width','class','classEncoder']
dfIris = pd.read_csv(args.inputPath, names=columns)

dfIrisCleanColumns = ['sepal_length','sepal_width','petal_length','petal_width','classEncoder']
dfIrisCleanData = []
dfIrisDirtyColumns =['sepal_length','sepal_width','petal_length','petal_width','class','classEncoder','messageError']
dfIrisDirtyData = []

for index, row in dfIris.iterrows():
    if(index == 0):
        continue
    
    if( 4.3 < float(row.sepal_length) > 7.9):    
        dfIrisDirtyData.append([row.sepal_length,row.sepal_width, row.petal_length, row.petal_width, row['class'], row.classEncoder, 'sepal_length está fora do range 4.3 à 7.9'])
        continue

    if( 2.0 < float(row.sepal_width) > 4.4):    
        dfIrisDirtyData.append([row.sepal_length,row.sepal_width, row.petal_length, row.petal_width, row['class'], row.classEncoder, 'sepal_width está fora do range 2.0 à 4.4'])
        continue

    if( 1.0 < float(row.petal_length) > 6.9):    
        dfIrisDirtyData.append([row.sepal_length,row.sepal_width, row.petal_length, row.petal_width, row['class'], row.classEncoder, 'petal_length está fora do range 1.0 à 6.9'])
        continue

    if( 0.1 < float(row.petal_width) > 2.5):    
        dfIrisDirtyData.append([row.sepal_length,row.sepal_width, row.petal_length, row.petal_width, row['class'], row.classEncoder, 'petal_width está fora do range 0.1 à 2.5'])
        continue

    if( 0 < float(row.classEncoder) > 2):    
        dfIrisDirtyData.append([row.sepal_length,row.sepal_width, row.petal_length, row.petal_width, row['class'], row.classEncoder, 'classEncoder está fora do range 0 à 2'])
        continue

    if(row['class'] not in ['Iris-setosa', 'Iris-versicolor', 'Iris-virginica']):    
        dfIrisDirtyData.append([row.sepal_length,row.sepal_width, row.petal_length, row.petal_width, row['class'], row.classEncoder, 'class não é valido'])
        continue

    dfIrisCleanData.append([row.sepal_length,row.sepal_width, row.petal_length, row.petal_width, row.classEncoder])

dfIrisClean = pd.DataFrame(data=dfIrisCleanData, columns=dfIrisCleanColumns)
dfIrisDirty = pd.DataFrame(data=dfIrisDirtyData, columns=dfIrisDirtyColumns)

dfIrisClean.to_csv('irisclean.csv', index=False)
dfIrisDirty.to_csv('irisdirty.csv', index=False)

#le = preprocessing.LabelEncoder()
#dfIris['classEncoder'] = le.fit_transform(dfIris['class'])
#print(dfIris.classEncoder.unique())
#dfIris.to_csv(args.outputPath, index=False)