import os
import warnings
import sys
import pandas as pd
import numpy as np

from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.neural_network import MLPClassifier
from sklearn.tree import DecisionTreeClassifier
from urllib.parse import urlparse
import mlflow
import mlflow.sklearn

import logging

logging.basicConfig(level=logging.WARN)
logger = logging.getLogger(__name__)

def eval_metrics(actual, pred):
    rmse = np.sqrt(mean_squared_error(actual, pred))
    mae = mean_absolute_error(actual, pred)
    r2 = r2_score(actual, pred)
    return rmse, mae, r2

warnings.filterwarnings("ignore")
np.random.seed(40)

csv_url = (
        "~/airflow/dags/etl_scripts/irisclean.csv"
)
try:
    #data = pd.read_csv(csv_url, sep=";")
    data = pd.read_csv(csv_url, sep=",")
except Exception as e:
    logger.exception(
        "Unable to download training & test CSV, check your internet connection. Error: %s", e
    )
#print (data['classEncoder'].value_counts())
#print(data['classEncoder'].drop_duplicates())

train, test = train_test_split(data)
train_x = train.drop(["classEncoder"], axis=1)
train_y = train[["classEncoder"]]
test_x = test.drop(["classEncoder"], axis=1)
test_y = test[["classEncoder"]]

try:
    idExperiment = mlflow.create_experiment('Classificacao-Iris-final-5')
except:
    idExperiment = mlflow.get_experiment_by_name('Classificacao-Iris-final-5').experiment_id

with mlflow.start_run(experiment_id=idExperiment):
    DTC = DecisionTreeClassifier().fit(train_x, train_y)
    predicted_qualities = DTC.predict(test_x)    
    mlflow.log_metric("ac", accuracy_score(test_y, predicted_qualities))

    tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme

    if tracking_url_type_store != "file":
        mlflow.sklearn.log_model(DTC, "model", registered_model_name="IrisDecisionTreeClassifier")
    else:
        mlflow.sklearn.log_model(DTC, "model")