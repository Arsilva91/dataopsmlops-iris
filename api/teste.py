import pickle
import json
import pandas as pd
import numpy as np
from sys import argv

model = pickle.load(open('model.pkl', 'rb'))
arr = np.array([[5.1,3.5,1.4,0.2],[5.9,3.0,5.1,1.8]])
pred = model.predict(arr)
print(pred)